package com.epam.spring;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.epam.helper.DatabaseCreator;

@Configuration
public class SpringConfigurationDataSource {
	private static String dbUrl = "jdbc:mysql://localhost/sql_lockertracker";
	private static String username = "root";
	private static String password = "root";

	@Bean
	public DataSource dataSource() {
	    try {
	        Class.forName("com.mysql.jdbc.Driver").newInstance();
	    } catch (Exception ex) {
	        System.err.println(ex.getMessage());
	    }
	    
		return new DriverManagerDataSource(dbUrl, username, password);
	}

	@Bean
	public DatabaseCreator databaseCreator(DataSource dataSource) {
		return new DatabaseCreator(dataSource);
	}

}
