package com.epam.spring;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.epam.dao.LockerDao;
import com.epam.daojdbcimpl.JdbcTemplateLockerDao;

@Configuration
public class SpringConfigurationJdbcTemplateDao {

	@Bean
	LockerDao lockerDao(DataSource dataSource) {
		return new JdbcTemplateLockerDao(dataSource);
	}

}
