package com.epam.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.dao.LockerDao;
import com.epam.model.Employee;
import com.epam.model.Locker;
import com.epam.service.LockerService;

@Controller
@SessionAttributes("employee")
public class TrackerController {

    private LockerDao lockerDao;

    @Autowired
    LockerService lockerService = new LockerService(lockerDao);

    @RequestMapping(value = "/tracking", method = RequestMethod.GET)
    public ModelAndView tracking(@ModelAttribute("employee") Employee employee, ModelAndView model) {

        List<Locker> emptyLockers = null;
        List<Locker> reservedLockers = null;
        Map<String, List<Locker>> lockerMap = new HashMap<>();

        emptyLockers = lockerService.loadEmptyLockerList();
        reservedLockers = lockerService.loadReservedLockerList();

        lockerService.loadEmployeeLockerId(employee, reservedLockers);
        lockerService.lockerHandling(employee, reservedLockers, emptyLockers);

        lockerService.clearDB();
        lockerService.saveLockerList(emptyLockers);
        lockerService.saveLockerList(reservedLockers);

        emptyLockers = lockerService.loadEmptyLockerList();
        reservedLockers = lockerService.loadReservedLockerList();

        lockerMap.put("emptyLockers", emptyLockers);
        lockerMap.put("reservedLockers", reservedLockers);

        return new ModelAndView("tracking", lockerMap);
    }

    @RequestMapping(value = "/emptyLockers", method = RequestMethod.POST)
    public @ResponseBody List<Locker> listAllLocker() throws FileNotFoundException, IOException {
        return lockerService.loadEmptyLockerList();
    }

    @RequestMapping(value = "/tracking", method = RequestMethod.POST)
    public ModelAndView trackingpost(@ModelAttribute("employee") Employee employee) {
        return new ModelAndView("redirect:tracking.html", "employee", employee);
    }

}
