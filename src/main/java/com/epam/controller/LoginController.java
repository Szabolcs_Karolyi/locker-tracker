package com.epam.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.model.Employee;

@Controller
@SessionAttributes("employee")
public class LoginController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class.getName());

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView login() {
        Employee employee = new Employee();

        return new ModelAndView("login", "employee", employee);
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ModelAndView updateEmployee(@ModelAttribute("employee") Employee employee, BindingResult result) {

        LOGGER.info("Employee logged in as: " + employee.getName());

        if (result.hasErrors()) {
            LOGGER.info("Error occured.");
            return new ModelAndView("login");
        }

        return new ModelAndView("redirect:tracking.html", "employee", employee);
    }
}
