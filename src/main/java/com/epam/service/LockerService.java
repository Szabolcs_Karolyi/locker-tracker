package com.epam.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;

import com.epam.controller.LoginController;
import com.epam.dao.LockerDao;
import com.epam.model.Employee;
import com.epam.model.Locker;;



@Service("lockerService")
@ContextConfiguration(locations = { "file:src/main/java/com/epam/spring/SpringConfigurationJdbcTemplateDao.java" })
public class LockerService {

    private LockerDao lockerDao;

    @Autowired
    public LockerService(LockerDao lockerDao) {
        this.lockerDao = lockerDao;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class.getName());

    public List<Locker> loadEmptyLockerList() {



//        String[] lineSplit = null;
//        File fileLocker = new File(filePath);
//
//        StringBuilder builder = new StringBuilder();
//
//        try (BufferedReader reader = new BufferedReader(new FileReader(fileLocker.getAbsoluteFile()))) {
//            String line = null;
//            int index = 0;
//            while ((line = reader.readLine()) != null && !line.equals("\\n")) {
//                builder.append(line);
//                lineSplit = line.split(":");
//                int id = Integer.parseInt(lineSplit[0]);
//                lockerList.add(new Locker());
//                lockerList.get(index).setId(id);
//                lockerList.get(index).setEmployeeName(lineSplit[1]);
//                index++;
//            }
//
//        } catch (FileNotFoundException e) {
//            LOGGER.error("File not found: " + filePath);
//        } catch (IOException e) {
//            LOGGER.error("IOException");
//        }

        List<Locker> lockerList = new ArrayList<>();
        lockerList = lockerDao.getEmptyLockers();
        LOGGER.info("Empty lockerlist loaded.");
        return lockerList;
    }


    public List<Locker> loadReservedLockerList() {

        List<Locker> lockerList = new ArrayList<>();
        lockerList = lockerDao.getReservedlockers();
        LOGGER.info("Reserved lockerlist loaded.");
        return lockerList;
    }

    public void saveLockerList(List<Locker> lockerList) {
//        File fileLocker = new File(filePath);
//
//        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileLocker))) {
//            for (Locker locker : lockerList) {
//                writer.write(locker.getId() + ":");
//                writer.write(locker.getEmployeeName() + "\n");
//            }
//            LOGGER.info("Locker list saved.");
//        } catch (FileNotFoundException e) {
//            LOGGER.error("File not found: " + filePath);
//        } catch (IOException e) {
//            LOGGER.error("IOException");
//        }

        lockerDao.saveLockers(lockerList);
    }

    public void clearDB() {
        lockerDao.clearDB();
    }

    public void loadEmployeeLockerId(Employee employee, List<Locker> reservedLockers) {

        if (!employee.isLoaded() && (employee.getLockerId() == employee.getNewLockerId())) {
            for (Locker locker : reservedLockers) {
                if (employee.getName().equals(locker.getEmployeeName())) {
                    employee.setLockerId(locker.getId());
                    employee.setNewLockerId(locker.getId());
                    LOGGER.info("User's locker id loaded: " + employee.getLockerId());
                    LOGGER.info("User's new locker id loaded: " + employee.getNewLockerId());
                    employee.setLoaded(true);
                }
            }
        }
    }

    public void lockerHandling(Employee employee, List<Locker> reservedLockers, List<Locker> emptyLockers) {

        Locker lockerDelete = null;
        Locker lockerAdd = null;
        if ((employee.getLockerId() == 0) && (employee.getLockerId() != employee.getNewLockerId())) {
            for (Locker locker : emptyLockers) {
                if (locker.getId() == employee.getNewLockerId()) {
                    lockerDelete = locker;
                    lockerAdd = new Locker();
                    lockerAdd.setId(employee.getNewLockerId());
                    lockerAdd.setEmployeeName(employee.getName());
                }
            }
            reservedLockers.add(lockerAdd);
            emptyLockers.remove(lockerDelete);
            LOGGER.info("Locker with the id: " + employee.getNewLockerId()
                    + " moved from emptyLockerList to reservedLockerList with the name: " + employee.getName());
            LOGGER.info("Employee without a locker reserved the locker with the id: " + employee.getNewLockerId());
            employee.setLockerId(employee.getNewLockerId());
        }

        if ((employee.getLockerId() != 0) && (employee.getNewLockerId() != 0)
                && (employee.getLockerId() != employee.getNewLockerId())) {

            for (Locker locker : emptyLockers) {
                if (locker.getId() == employee.getNewLockerId()) {
                    lockerDelete = locker;
                    lockerAdd = new Locker();
                    lockerAdd.setId(employee.getNewLockerId());
                    lockerAdd.setEmployeeName(employee.getName());
                }
            }

            reservedLockers.add(lockerAdd);
            emptyLockers.remove(lockerDelete);
            LOGGER.info("Locker with the id: " + employee.getNewLockerId()
                    + " moved from emptyLockerList to reservedLockerList with the name: " + employee.getName());

            for (Locker locker : reservedLockers) {
                if (locker.getId() == employee.getLockerId()) {
                    lockerDelete = locker;
                    lockerAdd = new Locker();
                    lockerAdd.setId(employee.getLockerId());
                    lockerAdd.setEmployeeName("empty");
                }
            }
            emptyLockers.add(lockerAdd);
            reservedLockers.remove(lockerDelete);
            LOGGER.info("Locker with the id: " + employee.getLockerId()
                    + " moved from reservedLockerList to emptyLockerList with the name: empty");
            LOGGER.info("Employee switched from locker id: " + employee.getLockerId() + " to locker id: "
                    + employee.getNewLockerId());
            employee.setLockerId(employee.getNewLockerId());
        }

        if (employee.getNewLockerId() == 0 && (employee.getLockerId() != employee.getNewLockerId())) {
            for (Locker locker : reservedLockers) {
                if (locker.getId() == employee.getLockerId()) {
                    lockerDelete = locker;
                    lockerAdd = new Locker();
                    lockerAdd.setId(employee.getLockerId());
                    lockerAdd.setEmployeeName("empty");
                }
            }
            emptyLockers.add(lockerAdd);
            reservedLockers.remove(lockerDelete);
            LOGGER.info("Locker with the id: " + employee.getLockerId()
                    + " moved from reservedLockerList to emptyLockerList");
            LOGGER.info("Employee unreserved the locker with the id: " + employee.getLockerId());
            employee.setLockerId(employee.getNewLockerId());
        }

    }
}
