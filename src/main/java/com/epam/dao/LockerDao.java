package com.epam.dao;

import java.util.List;

import com.epam.model.Locker;

public interface LockerDao {
    List<Locker> getEmptyLockers();

    List<Locker> getReservedlockers();

    void saveLockers(List<Locker> lockerList);

    void clearDB();
}
