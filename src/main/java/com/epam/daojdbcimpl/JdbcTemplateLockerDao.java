package com.epam.daojdbcimpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.epam.dao.LockerDao;
import com.epam.model.Locker;

public class JdbcTemplateLockerDao extends JdbcDaoSupport implements LockerDao {

    private static final String QUERY_GET_EMPTYLIST = "SELECT * FROM Lockers WHERE Name='empty' ORDER BY lockerID ASC";
    private static final String QUERY_GET_RESERVEDLIST = "SELECT * FROM Lockers WHERE Name NOT LIKE 'empty' ORDER BY lockerID ASC";
    private static final String QUERY_DELETE_LIST = "DELETE FROM Lockers";
    private static final String QUERY_INSERT_LIST = "INSERT INTO LOCKERS " +
            "(lockerID, name) VALUES (?, ?)";


    public JdbcTemplateLockerDao(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    public List<Locker> getEmptyLockers() {
        return getJdbcTemplate().query(QUERY_GET_EMPTYLIST, new Object[] {}, new LockerMapper());
    }

    @Override
    public List<Locker> getReservedlockers() {
        return getJdbcTemplate().query(QUERY_GET_RESERVEDLIST, new Object[] {}, new LockerMapper());
    }

    private static final class LockerMapper implements RowMapper<Locker> {
        @Override
        public Locker mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            Locker locker = new Locker();
            locker.setId(resultSet.getInt("lockerID"));
            locker.setEmployeeName(resultSet.getString("Name"));
            return locker;
        }
    }

    @Override
    public void clearDB() {
        getJdbcTemplate().execute(QUERY_DELETE_LIST);
    }

    @Override
    public void saveLockers(List<Locker> lockerList) {

        getJdbcTemplate().batchUpdate(QUERY_INSERT_LIST, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(java.sql.PreparedStatement ps, int i) throws SQLException {
                Locker locker = lockerList.get(i);
                ps.setInt(1, locker.getId());
                ps.setString(2, locker.getEmployeeName());
            }

            @Override
            public int getBatchSize() {
                return lockerList.size();
            }
        });
    }
}
