package com.epam.model;

public class Locker implements Comparable<Locker> {
   private String employeeName;
   private int id;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    @Override
    public String toString() {
        return id + " " + employeeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(Locker locker) {
        int id=locker.getId();
        /* For Ascending order*/
        return this.id-id;

        /* For Descending order do like this */
        //return compareage-this.studentage;
    }

}
