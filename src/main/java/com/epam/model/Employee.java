package com.epam.model;

public class Employee {
    private String name;
    private int lockerId;
    private int newLockerId;
    private boolean loaded;

    public boolean isLoaded() {
        return loaded;
    }
    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }
    public int getNewLockerId() {
        return newLockerId;
    }
    public void setNewLockerId(int newLockerId) {
        this.newLockerId = newLockerId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getLockerId() {
        return lockerId;
    }

    public void setLockerId(int lockerId) {
        this.lockerId = lockerId;
    }

    @Override
    public String toString() {
        return "Employee [name=" + name + "]";
    }

}
