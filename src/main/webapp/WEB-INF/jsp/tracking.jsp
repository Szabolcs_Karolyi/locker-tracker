<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>   
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">



<title>LockerTracker</title>
</head>
<body>

<h1></h1>

<h1>User: ${employee.name}</h1>
<h2>Locker id: ${employee.lockerId}</h2>

<br>
<h3>Empty lockers:</h3>
${emptyLockers}

<br>
<h3>Reserved lockers:</h3>
${reservedLockers}

<br>
Enter the locker id
<form:form commandName="employee">
	<form:select path="newLockerId" items="${emptyLockers}" itemValue="id" itemLabel="id"></form:select>
	<input type="submit" value="Enter"/>
</form:form>
<br>
<form:form commandName="employee">
			<form:input path="newLockerId" type="hidden" value="0"/>
			<input type="submit" value="Leave Locker"/>
</form:form>

<br>

<a href="http://localhost:8080/LockerTracker/login.html">Logout</a>

</body>
</html>