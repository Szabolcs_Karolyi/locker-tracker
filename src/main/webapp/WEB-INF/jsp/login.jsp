<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
<h1>Login</h1>


<form:form commandName="employee">
	<form:errors path="*" cssClass="errorblock" element="div" />
	<table>
		<tr>
			<td>Enter Username:</td>
			<td><form:input path="name" cssErrorClass="error" /></td>
			<td><form:errors path="name" cssClass="error" /></td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="submit" value="Login"/>
			</td>
		</tr>
		
	</table>
</form:form>


</body>
</html>