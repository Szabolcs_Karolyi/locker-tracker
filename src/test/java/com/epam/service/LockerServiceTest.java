package com.epam.service;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.dao.LockerDao;
import com.epam.model.Employee;
import com.epam.model.Locker;

@TestExecutionListeners( { DependencyInjectionTestExecutionListener.class })
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/com/epam/LockerServiceTest-context.xml" })
public class LockerServiceTest {

    @Autowired
    private LockerDao lockerDao;
    private LockerService lockerService;

    @Before
    public void doSetup() {
        lockerDao = mock(LockerDao.class);
        lockerService = new LockerService(lockerDao);
    }

    // LoadLockerId Tests
    //
    // Employee has a locker in reservedLockerList
    // Loaded: true
    // id == NewId
    // expected: no change in employee
    @Test
    public void LoadEmployeeLockerIdTest1() {

        Employee employee = new Employee();
        employee.setLoaded(true);
        employee.setName("Employee1");
        employee.setLockerId(0);
        employee.setNewLockerId(0);

        List<Locker> reservedLockers = new ArrayList<>();
        Locker locker = new Locker();
        locker.setEmployeeName("Employee1");
        locker.setId(1);
        reservedLockers.add(locker);

        int expectedLockerId = 0;
        int expectedNewLockerId = 0;
        boolean expectedLoaded = true;
        lockerService.loadEmployeeLockerId(employee, reservedLockers);
        Assert.assertEquals(expectedLockerId, employee.getLockerId());
        Assert.assertEquals(expectedNewLockerId, employee.getNewLockerId());
        Assert.assertEquals(expectedLoaded, employee.isLoaded());
    }

    // Employee has a locker in reservedLockerList
    // Loaded: false
    // id == NewId
    // expected: no change in employee
    @Test
    public void LoadEmployeeLockerIdTest2() {

        Employee employee = new Employee();
        employee.setLoaded(false);
        employee.setName("Employee1");
        employee.setLockerId(0);
        employee.setNewLockerId(1);

        List<Locker> reservedLockers = new ArrayList<>();
        Locker locker = new Locker();
        locker.setEmployeeName("Employee1");
        locker.setId(1);
        reservedLockers.add(locker);

        int expectedLockerId = 0;
        int expectedNewLockerId = 1;
        boolean expectedLoaded = false;
        lockerService.loadEmployeeLockerId(employee, reservedLockers);
        Assert.assertEquals(expectedLockerId, employee.getLockerId());
        Assert.assertEquals(expectedNewLockerId, employee.getNewLockerId());
        Assert.assertEquals(expectedLoaded, employee.isLoaded());
    }

    // Employee has a locker in reservedLockerList
    // lockerId = 1;
    // Loaded: false
    // id == NewId
    // expected: employee's lockerId = 1, loaded = true
    @Test
    public void LoadEmployeeLockerIdTest3() {

        Employee employee = new Employee();
        employee.setLoaded(false);
        employee.setName("Employee1");
        employee.setLockerId(0);
        employee.setNewLockerId(0);

        List<Locker> reservedLockers = new ArrayList<>();
        Locker locker = new Locker();
        locker.setEmployeeName("Employee1");
        locker.setId(1);
        reservedLockers.add(locker);

        int expectedLockerId = 1;
        int expectedNewLockerId = 1;
        boolean expectedLoaded = true;

        lockerService.loadEmployeeLockerId(employee, reservedLockers);

        Assert.assertEquals(expectedLockerId, employee.getLockerId());
        Assert.assertEquals(expectedNewLockerId, employee.getNewLockerId());
        Assert.assertEquals(expectedLoaded, employee.isLoaded());
    }

    // Lockerhandling Tests
    //
    // Employee had no locker and reserve a locker from the emptyList (0->1)
    @Test
    public void lockerHandlingTestReserve() {
        List<Locker> emptyLockers = new ArrayList<>();
        List<Locker> reservedLockers = new ArrayList<>();

        Employee employee = new Employee();
        employee.setName("Employee1");
        employee.setLockerId(0);
        employee.setNewLockerId(1);

        Locker locker1 = new Locker();
        locker1.setEmployeeName("empty");
        locker1.setId(1);
        emptyLockers.add(locker1);
        Locker locker2 = new Locker();
        locker2.setEmployeeName("empty");
        locker2.setId(2);
        emptyLockers.add(locker2);
        Locker locker3 = new Locker();
        locker3.setEmployeeName("empty");
        locker3.setId(3);
        emptyLockers.add(locker3);

        boolean actualLockerInEmptyList = false;
        boolean actualLockerInReservedList = false;

        lockerService.lockerHandling(employee, reservedLockers, emptyLockers);

        for (Locker locker : emptyLockers) {
            if (locker.getId() == employee.getNewLockerId()) {
                actualLockerInEmptyList = true;
            }
        }

        for (Locker locker : reservedLockers) {
            if ((locker.getId() == employee.getNewLockerId())
                    && (locker.getEmployeeName().equals(employee.getName()))) {
                actualLockerInReservedList = true;
            }
        }
        boolean expectedLockerInEmptyList = false;
        boolean expectedLockerInReservedList = true;
        Assert.assertEquals(expectedLockerInEmptyList, actualLockerInEmptyList);
        Assert.assertEquals(expectedLockerInReservedList, actualLockerInReservedList);
    }

    // Employee switch between lockers (1->2)
    @Test
    public void lockerHandlingTestSwitch() {
        List<Locker> emptyLockers = new ArrayList<>();
        List<Locker> reservedLockers = new ArrayList<>();

        Employee employee = new Employee();
        employee.setName("Employee1");
        employee.setLockerId(1);
        employee.setNewLockerId(2);

        Locker locker1 = new Locker();
        locker1.setEmployeeName("Employee1");
        locker1.setId(1);
        reservedLockers.add(locker1);
        Locker locker2 = new Locker();
        locker2.setEmployeeName("empty");
        locker2.setId(2);
        emptyLockers.add(locker2);
        Locker locker3 = new Locker();
        locker3.setEmployeeName("empty");
        locker3.setId(3);
        emptyLockers.add(locker3);

        boolean actualLockerInEmptyList = false;
        boolean actualLockerInReservedList = false;

        lockerService.lockerHandling(employee, reservedLockers, emptyLockers);

        for (Locker locker : emptyLockers) {
            if (locker.getId() == 1) {
                actualLockerInEmptyList = true;
            }
        }

        for (Locker locker : reservedLockers) {
            if ((locker.getId() == employee.getNewLockerId())
                    && (locker.getEmployeeName().equals(employee.getName()))) {
                actualLockerInReservedList = true;
            }
        }

        boolean expectedLockerInEmptyList = true;
        boolean expectedLockerInReservedList = true;
        Assert.assertEquals(expectedLockerInEmptyList, actualLockerInEmptyList);
        Assert.assertEquals(expectedLockerInReservedList, actualLockerInReservedList);
    }

    // Employee switch between lockers (1->0)
    @Test
    public void lockerHandlingTestLeave() {
        List<Locker> emptyLockers = new ArrayList<>();
        List<Locker> reservedLockers = new ArrayList<>();

        Employee employee = new Employee();
        employee.setName("Employee1");
        employee.setLockerId(1);
        employee.setNewLockerId(0);

        Locker locker1 = new Locker();
        locker1.setEmployeeName("Employee1");
        locker1.setId(1);
        reservedLockers.add(locker1);

        boolean actualLockerInEmptyList = false;
        boolean actualLockerInReservedList = false;

        lockerService.lockerHandling(employee, reservedLockers, emptyLockers);

        for (Locker locker : emptyLockers) {
            if (locker.getId() == 1) {
                actualLockerInEmptyList = true;
            }
        }

        for (Locker locker : reservedLockers) {
            if ((locker.getId() == employee.getNewLockerId())
                    && (locker.getEmployeeName().equals(employee.getName()))) {
                actualLockerInReservedList = true;
            }
        }

        boolean expectedLockerInEmptyList = true;
        boolean expectedLockerInReservedList = false;
        Assert.assertEquals(expectedLockerInEmptyList, actualLockerInEmptyList);
        Assert.assertEquals(expectedLockerInReservedList, actualLockerInReservedList);
    }

    // LoadLockerList Tests
    //
    @Test
    public void loadLockerListTest1() {

        List<Locker> actualEmptyLockerList = new ArrayList<>();
        List<Locker> actualReservedLockerList = new ArrayList<>();
        List<Locker> expectedEmptyLockerList = new ArrayList<>();
        List<Locker> expectedReservedLockerList = new ArrayList<>();

        Locker emptyLocker1 = new Locker();
        Locker emptyLocker2 = new Locker();
        Locker emptyLocker3 = new Locker();
        emptyLocker1.setEmployeeName("empty");
        emptyLocker2.setEmployeeName("empty");
        emptyLocker3.setEmployeeName("empty");
        emptyLocker1.setId(1);
        emptyLocker2.setId(2);
        emptyLocker3.setId(4);

        Locker reservedLocker1 = new Locker();
        Locker reservedLocker2 = new Locker();
        reservedLocker1.setEmployeeName("béla");
        reservedLocker2.setEmployeeName("gyula");
        reservedLocker1.setId(3);
        reservedLocker2.setId(5);


        actualEmptyLockerList = lockerService.loadEmptyLockerList();
        actualReservedLockerList = lockerService.loadReservedLockerList();


        expectedEmptyLockerList.add(emptyLocker1);
        expectedEmptyLockerList.add(emptyLocker2);
        expectedEmptyLockerList.add(emptyLocker3);
        expectedReservedLockerList.add(reservedLocker1);
        expectedReservedLockerList.add(reservedLocker2);

        int expectedId;
        String expectedEmployeeName;

        int index = 0;
        for (Locker locker : actualEmptyLockerList) {
            expectedId = expectedEmptyLockerList.get(index).getId();
            expectedEmployeeName = expectedEmptyLockerList.get(index).getEmployeeName();
            Assert.assertEquals(expectedId, locker.getId());
            Assert.assertEquals(expectedEmployeeName, locker.getEmployeeName());
            index++;
        }
        index = 0;
        for (Locker locker : actualReservedLockerList) {
            Assert.assertEquals(expectedReservedLockerList.get(index).getId(), locker.getId());
            Assert.assertEquals(expectedReservedLockerList.get(index).getEmployeeName(), locker.getEmployeeName());
            index++;
        }
    }


}
